﻿using MadPay.Common.ErrorAndMessage;
using MadPay.Data.Dtos.Site.Panel.Photos;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MadPay.Services.Upload.Interface
{
    public interface IUploadService
    {
        Task<FileUploadDto> UploadToCloudinary(IFormFile file, string userId); 
        Task<FileUploadDto> UploadToLocal(IFormFile file, string userId, string WebRootPath, string UrlBegan, string _url); 
        Task<FileUploadDto> UploadFile(IFormFile file, string userId, string WebRootPath, string UrlBegan, string _url);

        FileUploadDto RemoveFileFromCloudinary(string publicId);
        FileUploadDto RemoveFileFromLocal(string photoName, string WebRootPath, string filePath);

        ReturnMessage CreateDirectory(string WebRootPath, string _url);
    }
}
