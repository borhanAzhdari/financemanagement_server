﻿using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using MadPay.Common.ErrorAndMessage;
using MadPay.Data.DatabaseContext;
using MadPay.Data.Dtos.Site.Panel.Photos;
using MadPay.Data.Models;
using MadPay.Repo.Infrastructure;
using MadPay.Services.Upload.Interface;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MadPay.Services.Upload.Service
{
    public class UploadService : IUploadService
    {
        private readonly IUnitOfWork<MadPayDbContext> _db;
        private readonly Cloudinary _cloudinary;
        private readonly Setting _setting;

        public UploadService(IUnitOfWork<MadPayDbContext> dbContext)
        {
            _db = dbContext;
            _setting = _db.Repository<Setting>().GetById((short)11);
            Account acc = new Account(_setting.CloudinaryCloudName, _setting.CloudinaryAPIKey, _setting.CloudinaryAPISecret);
            _cloudinary = new Cloudinary(acc);
        }

        public async Task<FileUploadDto> UploadToCloudinary(IFormFile file, string userId)
        {
            var uploadResult = new ImageUploadResult();

            if (file.Length > 0)
            {
                try
                {
                    using (var stream = file.OpenReadStream())
                    {
                        var uploadParams = new ImageUploadParams()
                        {
                            File = new FileDescription(file.Name, stream),
                            Transformation = new Transformation().Width(250).Height(250).Crop("fill").Gravity("face"),
                            Folder = "ProfilePic/" + userId
                        };
                        uploadResult = await _cloudinary.UploadAsync(uploadParams);
                        if (uploadResult.Error == null)
                        {
                            return new FileUploadDto()
                            {
                                Status = true,
                                publicId = uploadResult.PublicId,
                                Url = uploadResult.Url.ToString(),
                                Message = "آپلود با موفقیت انجام شد",
                                LocalUploaded = false
                            };
                        }
                        else
                        {
                            return new FileUploadDto()
                            {
                                Status = false,
                                Message = uploadResult.Error.Message
                            };
                        }
                    }
                }
                catch (Exception ex)
                {
                    return new FileUploadDto()
                    {
                        Status = false,
                        Message = ex.Message
                    };
                }
            }
            else
            {
                return new FileUploadDto()
                {
                    Status = false,
                    Message = "فایلی برای آپلود یافت نشد"
                };
            }
            //photo.Url = uploadResult.Url.ToString();
            //photo.PublicId = uploadResult.PublicId;
        }

        public async Task<FileUploadDto> UploadToLocal(IFormFile file, string userId, string WebRootPath, string UrlBegan, string _url)
        {
            if (file.Length > 0)
            {
                try
                {
                    string fileName = Path.GetFileName(file.FileName);
                    string fileExtension = Path.GetExtension(fileName);
                    string fileNewName = string.Format("{0}{1}", userId, fileExtension);
                    string path = Path.Combine(WebRootPath, _url);
                    string fullPath = Path.Combine(path, fileNewName);
                    var dirRes = CreateDirectory(WebRootPath, _url);
                    if (!dirRes.status)
                    {
                        return new FileUploadDto()
                        {
                            Status = false,
                            Message = dirRes.message
                        };
                    }

                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }

                    return new FileUploadDto()
                    {
                        Status = true,
                        LocalUploaded = true,
                        Message = "فایل با موفقیت در لوکال آپلود شد",
                        publicId = "0",
                        Url = $"{UrlBegan}/{"wwwroot/" + _url.Split('\\').Aggregate("", (current, str) => current + (str + "/")) + fileNewName}"
                    };
                }
                catch (Exception ex)
                {
                    return new FileUploadDto()
                    {
                        Status = false,
                        Message = ex.Message
                    };
                }
            }
            else
            {
                return new FileUploadDto()
                {
                    Status = false,
                    Message = "فایلی برای آپلود یافت نشد"
                };
            }
        }

        public async Task<FileUploadDto> UploadFile(IFormFile file, string userId, string WebRootPath, string UrlBegan, string _url)
        {
            if (_setting.UploadLocal)
            {
                return await UploadToLocal(file, userId, WebRootPath, UrlBegan, _url);
            }
            else
            {
                return await UploadToCloudinary(file, userId);
            }
        }

        public FileUploadDto RemoveFileFromCloudinary(string publicId)
        {
            var deleteParams = new DeletionParams(publicId);
            var deleteResult = _cloudinary.Destroy(deleteParams);
            if (deleteResult.Result.ToLower() == "ok")
            {
                return new FileUploadDto()
                {
                    Status = true,
                    Message = "فایل با موفقیت حذف شد"
                };
            }
            else
            {
                return new FileUploadDto()
                {
                    Status = false
                };
            }
        }

        public FileUploadDto RemoveFileFromLocal(string photoName, string WebRootPath, string filePath)
        {
            string path = Path.Combine(WebRootPath, filePath);
            string fullPath = Path.Combine(path, photoName);
            if (File.Exists(fullPath))
            {
                File.Delete(fullPath);
                return new FileUploadDto()
                {
                    Status = true,
                    Message = "فایل با موفقیت حذف شد"
                };
            }
            else
            {
                return new FileUploadDto()
                {
                    Status = true,
                    Message = "فایل وجود نداشت"
                };
            }
        }

        public ReturnMessage CreateDirectory(string WebRootPath, string _url)
        {
            try
            {
                var path = Path.Combine(WebRootPath, _url);
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                return new ReturnMessage
                {
                    status = true
                };

            }
            catch (Exception ex)
            {
                return new ReturnMessage
                {
                    status = false,
                    message = ex.Message
                };
            }
        }
    }
}