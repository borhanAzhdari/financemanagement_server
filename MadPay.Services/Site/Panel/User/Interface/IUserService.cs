﻿using MadPay.Data.Dtos.Site.Panel.Users;
using MadPay.Data.Models;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace MadPay.Services.Site.Panel.Interface
{
    public interface IUserService
    {
        Task<User> GetUserForChangePassword(string id , string password);
        Task<bool> UpdateUserPass(User user , string newPassword);
    }
}
