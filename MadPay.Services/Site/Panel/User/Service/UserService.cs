﻿using MadPay.Common.Helpers.Interface;
using MadPay.Data.DatabaseContext;
using MadPay.Data.Models;
using MadPay.Repo.Infrastructure;
using MadPay.Services.Site.Panel.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MadPay.Services.Site.Panel.Service
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork<MadPayDbContext> _db;
        private readonly IUtilities _utilities;
        public UserService(IUnitOfWork<MadPayDbContext> db, IUtilities utilities)
        {
            _db = db;
            _utilities = utilities;
        }
        public async Task<Data.Models.User> GetUserForChangePassword(string id, string password)
        {
            var user = await _db.Repository<User>().GetByIdAsync(id);
            if (user == null)
                return null;
            if (!_utilities.VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                return null;
            return user;
        }

        public async Task<bool> UpdateUserPass(User user, string newPassword)
        {
            byte[] passwordHash, passwordSalt;
            _utilities.CreatePasswordHash(newPassword, out passwordHash, out passwordSalt);
            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;
            _db.Repository<User>().Update(user);
            return await _db.SaveAsync();
        }
    }
}
