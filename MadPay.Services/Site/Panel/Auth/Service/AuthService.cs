﻿using MadPay.Common.Helpers.Interface;
using MadPay.Common.Helpers.Utilities;
using MadPay.Data.DatabaseContext;
using MadPay.Data.Models;
using MadPay.Repo.Infrastructure;
using MadPay.Services.Site.Panel.Auth.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MadPay.Services.Site.Panel.Auth.Service
{
    public class AuthService : IAuthService
    {
        private readonly IUnitOfWork<MadPayDbContext> _db;
        private readonly IUtilities _utilities;

        public AuthService(IUnitOfWork<MadPayDbContext> dbContext, IUtilities _Utilities)
        {
            _db = dbContext;
            _utilities = _Utilities;
        }

        public async Task<User> LoginAsync(string username, string password)
        {
            var users = await _db.Repository<User>().GetManyAsync(p => p.UserName == username, null, "Photos");
            var user = users.SingleOrDefault();
            if (user == null)
            {
                return null;
            }
            if (!_utilities.VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                return null;
            return user;
        }

        public async Task<User> RegisterAsync(User user, Photo photo, string password)
        {
            byte[] passwordHash, passwordSalt;
            _utilities.CreatePasswordHash(password, out passwordHash, out passwordSalt);
            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;
            await _db.Repository<User>().InsertAsync(user);
            await _db.Repository<Photo>().InsertAsync(photo);

            if (await _db.SaveAsync())
            {
                return user;
            }
            else
            {
                return null;
            }
        }
    }
}