﻿using MadPay.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MadPay.Services.Site.Panel.Auth.Interface
{
    public interface IAuthService
    {
        Task<User> RegisterAsync(User user,Photo photo,string password);
        Task<User> LoginAsync(string username , string password);  
    }
}
