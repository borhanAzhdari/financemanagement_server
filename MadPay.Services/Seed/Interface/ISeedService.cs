﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MadPay.Services.Seed.Interface
{
    public interface ISeedService
    {
        void SeedUsers();
    }
}
