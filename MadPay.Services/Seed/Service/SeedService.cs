﻿using MadPay.Common.Helpers.Interface;
using MadPay.Common.Helpers.Utilities;
using MadPay.Data.DatabaseContext;
using MadPay.Data.Models;
using MadPay.Repo.Infrastructure;
using MadPay.Services.Seed.Interface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MadPay.Services.Seed.Service
{
    public class SeedService : ISeedService
    {
        private readonly IUnitOfWork<MadPayDbContext> _db;
        private readonly IUtilities _utilities;
        public SeedService(IUnitOfWork<MadPayDbContext> dbContext, IUtilities utilities)
        {
            _db = dbContext;
            _utilities = utilities;
        }

        public void SeedUsers()
        {
            var userData = System.IO.File.ReadAllText(@"wwwroot\Files\Json\Seed\UserDataSeed.json");
            var users = JsonConvert.DeserializeObject<IList<User>>(userData);
            foreach (var user in users)
            {
                byte[] passwordHash, passwordSalt;
                _utilities.CreatePasswordHash("1", out passwordHash, out passwordSalt);
                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;

                user.UserName = user.UserName.ToLower();
                _db.Repository<User>().Insert(user);
            }
            _db.Save();
        }
    }
}
