﻿using MadPay.Data.DatabaseContext;
using MadPay.Data.Models;
using MadPay.Repo.Infrastructure;
using MadPay.Repo.Repositories.MainDB.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MadPay.Repo.Repositories.MainDB.Repo
{
    public class UserRepository : Repository<User> ,IUserRepository
    {
        private readonly DbContext _db;

        public UserRepository(DbContext dbContext) : base(dbContext)
        {
            _db ??= (MadPayDbContext)_db;
        }

        public async Task<bool> UserExist(string username)
        {

            if (await GetAsync(u => u.UserName == username) != null)
                return true;

            return false;
        }
    }
}