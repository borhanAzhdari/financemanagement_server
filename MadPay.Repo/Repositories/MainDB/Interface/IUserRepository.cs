﻿using MadPay.Data.Models;
using MadPay.Repo.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MadPay.Repo.Repositories.MainDB.Interface
{
    public interface IUserRepository : IRepository<User>
    {
        Task<bool>UserExist(string username);
    }
}
