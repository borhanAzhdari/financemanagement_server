﻿using MadPay.Repo.Repositories.MainDB.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MadPay.Repo.Infrastructure
{
    public interface IUnitOfWork<TContext> : IDisposable where TContext: DbContext
    {
        IRepository<TEntity> Repository<TEntity>() where TEntity : class;

        IUserRepository UserRepository { get; }
        bool Save();
        Task<bool> SaveAsync();
    }
}