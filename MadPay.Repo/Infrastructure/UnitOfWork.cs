﻿using MadPay.Data.DatabaseContext;
using MadPay.Repo.Repositories.MainDB.Interface;
using MadPay.Repo.Repositories.MainDB.Repo;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MadPay.Repo.Infrastructure
{
    public class UnitOfWork<TContext> : IUnitOfWork<TContext> where TContext : DbContext
    {
        #region Ctor
        protected readonly DbContext _db;
        //private readonly MadPayDbContext _db;
        public UnitOfWork(TContext tContext)
        {
            this._db = tContext;
        }
        #endregion

        private readonly Dictionary<Type, object> _repositories = new Dictionary<Type, object>();
        public Dictionary<Type, object> Repositories
        {
            get { return _repositories; }
            set { Repositories = value; }
        }
        public IRepository<TEntity> Repository<TEntity>() where TEntity : class
        {
            if (Repositories.Keys.Contains(typeof(TEntity)))
            {
                return Repositories[typeof(TEntity)] as IRepository<TEntity>;
            }

            IRepository<TEntity> repo = new Repository<TEntity>(_db);
            Repositories.Add(typeof(TEntity), repo);
            return repo;
        }
        private IUserRepository userRepository;
        public IUserRepository UserRepository
        {
            get
            {
                if (userRepository == null)
                {
                    userRepository = new UserRepository(_db);
                }
                return userRepository;
            }
        }
        #region save
        public bool Save()
        {
            // _cleanStrings();
            if (_db.SaveChanges() > 0)
                return true;
            else
                return false;
        }
        public async Task<bool> SaveAsync()
        {
            // _cleanStrings();
            if (await _db.SaveChangesAsync() > 0)
                return true;
            else
                return false;
        }
        //private void _cleanStrings()
        //{
        //    var changedEntities = _db.ChangeTracker.Entries()
        //        .Where(p => p.State == EntityState.Added || p.State == EntityState.Modified);
        //    foreach (var item in changedEntities)
        //    {
        //        if (item.Entity == null)
        //            continue;

        //        var properties = item.Entity.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
        //            .Where(p => p.CanRead && p.CanWrite && p.PropertyType == typeof(string));

        //        foreach (var property in properties)
        //        {
        //            var propName = property.Name;
        //            var val = (string)property.GetValue(item.Entity, null);

        //            if (val.HasValue())
        //            {
        //                var newVal = val.CleanString();
        //                if (newVal == val)
        //                    continue;
        //                property.SetValue(item.Entity, newVal, null);
        //            }
        //        }
        //    }
        //    {

        //    }
        //}
        #endregion

        #region dispose
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
            }
            disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        ~UnitOfWork()
        {
            Dispose(false);
        }
        #endregion
    }
}