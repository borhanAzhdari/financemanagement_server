﻿using AutoMapper;
using MadPay.Common.Helpers.Utilities.Extensions;
using MadPay.Data.Dtos.Site.Panel.BankCards;
using MadPay.Data.Dtos.Site.Panel.Photos;
using MadPay.Data.Dtos.Site.Panel.Users;
using MadPay.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MadPay.Presentation.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<User, UserForListDto>();
            CreateMap<User, UserForDetailsDto>().ForMember(dest => dest.PhotoUrl, opt =>
               {
                   opt.MapFrom(src => src.Photos.FirstOrDefault(p => p.IsMain).Url);
               })
                .ForMember(dest => dest.Age, opt =>
                 {
                     opt.MapFrom(src => src.DateOfBirth.ToAge());
                 });
            CreateMap<Photo, PhotoForUserDetailedDto>();
            CreateMap<Photo,PhotoForReturnDTO>();
            CreateMap<PhotoForUserProfileDTO,Photo>();
            CreateMap<BankCard, BankCardForUserDetailedDto>();
            CreateMap<UserForUpdateDto, User>();

        }
    }
}
