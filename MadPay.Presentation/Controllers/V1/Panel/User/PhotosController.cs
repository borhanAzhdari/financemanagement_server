﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using MadPay.Common.Helpers.AppSetting;
using MadPay.Common.Helpers.Interface;
using MadPay.Data.DatabaseContext;
using MadPay.Data.Dtos.Site.Panel.Photos;
using MadPay.Data.Models;
using MadPay.Repo.Infrastructure;
using MadPay.Services.Upload.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace MadPay.Presentation.Controllers.V1.Panel.User
{
    [Authorize]
    // [ApiExplorerSettings(GroupName ="Site")]
    [Route("Site/Admin/{userId}/photos")]
    [ApiController]
    public class PhotosController : ControllerBase
    {
        private readonly IUnitOfWork<MadPayDbContext> _db;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _env;
        private readonly IUploadService _uploadService;
        private readonly IUtilities _utilities;
        public PhotosController(IUnitOfWork<MadPayDbContext> db, IMapper mapper, IUploadService uploadService, IWebHostEnvironment env, IUtilities utilities)
        {
            _db = db;
            _mapper = mapper;
            _uploadService = uploadService;
            _env = env;
            _utilities = utilities;
        }

        [HttpPost]
        public async Task<IActionResult> ChangeUserPhoto(string userId, [FromForm] PhotoForUserProfileDTO photo)
        {
            if (userId != User.FindFirst(ClaimTypes.NameIdentifier).Value)
                return Unauthorized("شما اجازه تغییر عکس پروفایل را ندارید");

            // var uploadRes = _uploadService.UploadToCloudinary(photo.File,userId);
            var uplaodRes = await _uploadService.UploadFile(photo.File, userId, _env.WebRootPath,
                $"{Request.Scheme ?? ""}://{Request.Host.Value ?? ""}{Request.PathBase.Value ?? ""}",
                "Files\\Pic\\Profile\\" + DateTime.Now.Year + "\\" + DateTime.Now.Month + "\\" + DateTime.Now.Day
            );
            if (uplaodRes.Status)
            {
                photo.Url = uplaodRes.Url;
                if (uplaodRes.LocalUploaded)
                    photo.PublicId = "1";
                else
                    photo.PublicId = uplaodRes.publicId;

                //var createPhoto = _mapper.Map<Photo>(photo);
                //createPhoto.UserId = userId;

                var oldPhoto = await _db.Repository<Photo>().GetAsync(p => p.UserId == userId && p.IsMain);

                if (oldPhoto.publicId != "2")
                {
                    if (oldPhoto.publicId != null && oldPhoto.publicId != "0" && oldPhoto.publicId != "1")
                    {
                        _uploadService.RemoveFileFromCloudinary(oldPhoto.publicId);
                    }
                    if (oldPhoto.publicId == photo.PublicId && photo.Url.Split('/').Last() != oldPhoto.Url.Split('/').Last())
                    {
                        _uploadService.RemoveFileFromLocal(
                            oldPhoto.Url.Split('/').Last(),
                            _env.WebRootPath,
                            _utilities.FindLocalPathFromUrl(oldPhoto.Url).Replace("wwwroot\\", ""));
                    }
                    if (oldPhoto.publicId == "1" && photo.PublicId != "1")
                    {
                        _uploadService.RemoveFileFromLocal(
                           oldPhoto.Url.Split('/').Last(),
                           _env.WebRootPath,
                           _utilities.FindLocalPathFromUrl(oldPhoto.Url).Replace("wwwroot\\", ""));
                    }
                }
                _mapper.Map(photo, oldPhoto);

                _db.Repository<Photo>().Update(oldPhoto);

                if (await _db.SaveAsync())
                {
                    var photoForReturn = _mapper.Map<PhotoForReturnDTO>(oldPhoto);
                    return CreatedAtRoute("GetPhoto", new { id = oldPhoto.Id }, photoForReturn);
                }
                else
                {
                    return BadRequest("خطایی در اپلود دوباره امتحان کنید");
                }
            }
            else
            {
                return BadRequest(uplaodRes.Message);
            }
        }

        [HttpGet("{id}", Name = "GetPhoto")]
        public async Task<IActionResult> GetPhoto(string id)
        {
            var photoFromRepo = await _db.Repository<Photo>().GetByIdAsync(id);
            var photo = _mapper.Map<PhotoForReturnDTO>(photoFromRepo);
            return Ok(photo);
        }
    }
}