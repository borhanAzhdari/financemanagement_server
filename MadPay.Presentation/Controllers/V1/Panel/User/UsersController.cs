﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using MadPay.Common.ErrorAndMessage;
using MadPay.Data.DatabaseContext;
using MadPay.Data.Dtos.Site.Panel.Users;
using MadPay.Data.Models;
using MadPay.Repo.Infrastructure;
using MadPay.Services.Site.Panel.Auth.Interface;
using MadPay.Services.Site.Panel.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace MadPay.Presentation.Controllers
{
    [Route("Site/Panel/[Controller]")]
    //[ApiVersion("1")]
    [ApiController]
    [Authorize]
    //[ApiExplorerSettings(GroupName ="Panel")]

    public class UsersController : ControllerBase
    {
        private readonly IUnitOfWork<MadPayDbContext> _db; 
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly ILogger<UsersController> _logger;
        public UsersController(IUnitOfWork<MadPayDbContext> db, IMapper mapper, IUserService userService, ILogger<UsersController> logger)
        {
            _db = db;
            _mapper = mapper;
            _userService = userService;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> GetUsers()
        {
            var users = await _db.Repository<User>().GetManyAsync(null, null, "Photos,BankCards");
            var usersToReturn = _mapper.Map<IEnumerable<UserForListDto>>(users);
            return Ok(usersToReturn);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser(string id)
        {
            //if (ClaimTypes.NameIdentifier == id)
            //{
            var user = await _db.Repository<User>().GetManyAsync(p => p.Id == id, null, "Photos");
            var userToReturn = _mapper.Map<UserForDetailsDto>(user.SingleOrDefault());
            return Ok(userToReturn);
            //}
            //else
            //{
            //    return Unauthorized("شما به این اطلاعات دسترسی ندارید");
            //}
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateUser(string id, UserForUpdateDto user)
        {
            if (id != User.FindFirst(ClaimTypes.NameIdentifier).Value)
            {
                _logger.LogError($"{id} : Your not Allowed to Edit {user.Name} ! ");
                return Unauthorized("شما اجازه ویرایش ندارید !");
            }
            var userFromRepo = await _db.Repository<User>().GetByIdAsync(id);
            _mapper.Map(user, userFromRepo);
            _db.Repository<User>().Update(userFromRepo);
            if (await _db.SaveAsync())
            {
                return NoContent();
            }
            else
            {
                return Unauthorized(new ReturnMessage
                {
                    status = false,
                    title = "خطا",
                    message = $"ویرایش برای کاربر {user.Name} انجام نشد"
                });
            }
        }

        [Route("ChangeUserPassword/{id}")]
        [HttpPut]
        public async Task<IActionResult> ChangeUserPassword(string id, PasswordForChange passwordForChange)
        {
            var user = await _userService.GetUserForChangePassword(id, passwordForChange.OldPassword);
            if (user == null)
                return Unauthorized(new ReturnMessage
                {
                    status = false,
                    title = "خطا",
                    message = "پسورد نادرست می باشد "
                });
            if (await _userService.UpdateUserPass(user, passwordForChange.NewPassword))
            {
                return NoContent();
            }
            else
            {
                return Unauthorized(new ReturnMessage
                {
                    status = false,
                    title = "خطا",
                    message = $"تغییر رمز برای کاربر {user.Name} انجام نشد"
                });
            }

        }
    }
}