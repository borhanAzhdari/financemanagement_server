﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using MadPay.Common.ErrorAndMessage;
using MadPay.Common.Helpers.Interface;
using MadPay.Data.DatabaseContext;
using MadPay.Data.Dtos.Site.Panel.Auth;
using MadPay.Data.Dtos.Site.Panel.Users;
using MadPay.Data.Models;
using MadPay.Repo.Infrastructure;
using MadPay.Repo.Repositories.MainDB.Interface;
using MadPay.Services.Site.Panel.Auth.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace MadPay.Presentation.Controllers
{
    [Route("Site/Panel/[Controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IUnitOfWork<MadPayDbContext> _db;
        private readonly IAuthService _authService;
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;

        public AuthController(IUnitOfWork<MadPayDbContext> db, IAuthService authService, IConfiguration config, IMapper mapper)
        {
            _db = db;
            _authService = authService;
            _config = config;
            _mapper = mapper;
        }

        [HttpPost("Register")]
        public async Task<IActionResult> Register(UserForRegisterDto user)
        {
            user.UserName = user.UserName.ToLower();
            if (await _db.UserRepository.UserExist(user.UserName))
                return BadRequest("این نام کاربری وجود دارد");

            var userCreate = new User
            {
                UserName = user.UserName,
                Name = user.Name,
                PhoneNumber = user.PhoneNumber,
                Address = "",
                City = "",
                DateOfBirth = DateTime.Now,
                IsActive = true,
                Status = true
            };
            var createPhoto = new Photo
            {
                UserId = userCreate.Id,
                Url = "https://res.cloudinary.com/du4rr032j/image/upload/v1602413304/1200px-Google_Contacts_icon.svg_xltmih.png",
                Description = "pic profile",
                Alt = "Profile Pic",
                IsMain = true,
                publicId = "0"
            };
            var _createdUser = await _authService.RegisterAsync(userCreate, createPhoto, user.Password);
            return Ok(user);
        }

        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<IActionResult> Login(UserForLoginDto user)
        {
            var userforLogin = await _authService.LoginAsync(user.UserName, user.Password);
            if (userforLogin == null)
                return Unauthorized(new ReturnMessage
                {
                    message = "نام کاربری و یا پسورد اشتباه است",
                    status = false,
                    title = "خطای لاگین "
                });

            #region Claims
            //Claims For Login
            var Claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier,userforLogin.Id.ToString()),
                new Claim(ClaimTypes.Name,userforLogin.UserName)
            };
            //Create TokenDescriptor
            var Key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.GetSection("AppSettings:Token").Value));
            var creds = new SigningCredentials(Key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDes = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(Claims),
                Expires = user.IsRemember ? DateTime.Now.AddDays(1) : DateTime.Now.AddHours(2),
                SigningCredentials = creds
            };
            //Create Token 
            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDes);
            var userForStorage = _mapper.Map<UserForDetailsDto>(userforLogin);
            #endregion
            //That Return Token
            return Ok(new
            {
                token = tokenHandler.WriteToken(token),
                user = userForStorage
            });
        }
    }
}