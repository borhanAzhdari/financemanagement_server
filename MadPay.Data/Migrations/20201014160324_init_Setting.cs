﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MadPay.Data.Migrations
{
    public partial class init_Setting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Settings",
                columns: table => new
                {
                    Id = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    CloudinaryCloudName = table.Column<string>(nullable: false),
                    CloudinaryAPIKey = table.Column<string>(nullable: false),
                    CloudinaryAPISecret = table.Column<string>(nullable: false),
                    UploadLocal = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Settings", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Settings");
        }
    }
}
