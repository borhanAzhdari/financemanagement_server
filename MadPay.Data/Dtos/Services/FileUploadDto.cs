﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MadPay.Data.Dtos.Site.Panel.Photos
{
    public class FileUploadDto
    {
        public bool Status { get; set; }
        public string Url { get; set; }
        public string publicId { get; set; } = "0";
        public string Message { get; set; }
        public bool LocalUploaded { get; set; }
    }
}
