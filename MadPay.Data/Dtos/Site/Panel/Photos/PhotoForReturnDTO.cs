﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MadPay.Data.Dtos.Site.Panel.Photos
{
    public class PhotoForReturnDTO
    {
        public string Url { get; set; }
        public string Description { get; set; }
        public string Alt { get; set; }
    }
}
