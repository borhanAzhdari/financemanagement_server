﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MadPay.Data.Dtos.Site.Panel.Users
{
    public class PasswordForChange
    {
        [Required]
        public string OldPassword { get; set; }
        [Required]
        [StringLength(10,MinimumLength =4,ErrorMessage ="پسورد باید بین 4 رقم تا 10 رقم باشد.")]
        public string NewPassword { get; set; }
    }
}
