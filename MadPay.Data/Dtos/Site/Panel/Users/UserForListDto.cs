﻿using MadPay.Data.Dtos.Site.Panel.BankCards;
using MadPay.Data.Dtos.Site.Panel.Photos;
using System;
using System.Collections.Generic;
using System.Text;

namespace MadPay.Data.Dtos.Site.Panel.Users
{
   public class UserForListDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }

        public ICollection<PhotoForUserDetailedDto> Photos { get; set; }
        public ICollection<BankCardForUserDetailedDto> BankCards { get; set; }
    }
}
