﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MadPay.Data.Dtos.Site.Panel.Auth
{
    public class UserForLoginDto
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        //[StringLength(10,MinimumLength =4,ErrorMessage ="پسورد وارد شده باید 4 تا 10 رقم باشد")]
        public string Password { get; set; }
        public bool IsRemember { get; set; }
    }
}
