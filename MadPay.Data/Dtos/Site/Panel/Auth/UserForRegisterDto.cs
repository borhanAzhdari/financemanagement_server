﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Authentication.ExtendedProtection;
using System.Text;

namespace MadPay.Data.Dtos.Site.Panel.Auth
{
   public class UserForRegisterDto
    {
        [Required]
        public string Name { get; set; }
        [Required]
        [EmailAddress(ErrorMessage ="لطفا ایمیل وارد کنید")]
        public string UserName { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        [StringLength(10, MinimumLength = 4, ErrorMessage = "پسورد وارد شده باید 4 تا 10 رقم باشد")]
        public string Password { get; set; }
    }
}
