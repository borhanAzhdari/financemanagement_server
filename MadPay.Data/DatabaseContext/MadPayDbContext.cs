﻿using MadPay.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace MadPay.Data.DatabaseContext
{
    public class MadPayDbContext : DbContext
    {
        //protected override void OnConfiguring(DbContextOptionsBuilder optionbuilder)
        //{
        //    optionbuilder.UseSqlServer(@"Data Source=DESKTOP-0KBC7QR\SQL2019; Initial Catalog=MadPaydb; User=sa; Password=b100; MultipleActiveResultSets = True");
        //}
        public MadPayDbContext(DbContextOptions<MadPayDbContext> opt) : base(opt)
        { }
        public MadPayDbContext()
        { }

        public DbSet<User> Users { get; set; }
        public DbSet<BankCard> bankCards { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<Setting> Settings { get; set; }
    }
}
