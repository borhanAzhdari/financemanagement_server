﻿using MadPay.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using ZNetCS.AspNetCore.Logging.EntityFrameworkCore;

namespace MadPay.Data.DatabaseContext
{
    public class LoggedDbContext : DbContext
    {
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer(@"Data Source=DESKTOP-0KBC7QR\\SQL2019; Initial Catalog=LogPaydb; User=sa; Password=b100; MultipleActiveResultSets = True");
        //}
        public LoggedDbContext(DbContextOptions<LoggedDbContext> opt) : base(opt)
        { }
        public LoggedDbContext()
        { }

        public DbSet<ExtendedLog> Logs { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            LogModelBuilderHelper.Build(modelBuilder.Entity<ExtendedLog>());
            modelBuilder.Entity<ExtendedLog>().ToTable("ExtendedLog");
        }
    }
}