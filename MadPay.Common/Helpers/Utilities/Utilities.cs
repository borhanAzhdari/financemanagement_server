﻿using MadPay.Common.Helpers.Interface;
using Microsoft.AspNetCore.Http;
using System.Linq;
using System.Text;

namespace MadPay.Common.Helpers.Utilities
{
    public class Utilities : IUtilities
    {
        public void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
            }
        }
        public bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hamc = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var computedHash = hamc.ComputeHash(Encoding.UTF8.GetBytes(password));

                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != passwordHash[i])
                        return false;
                }
            }
            return true;
        }

        public string FindLocalPathFromUrl(string url)
        {
            var arry = url.Replace("https://", "").Replace("http://", "")
                 .Split('/').Skip(1).SkipLast(1);

            return (arry.Aggregate("", (current, item) => current + (item + "\\"))).TrimEnd('\\');

        }
        public bool IsFile(IFormFile file)
        {
            if (file != null)
            {
                if (file.Length > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }
    }
}
