﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MadPay.Common.Helpers.Interface
{
    public interface IUtilities
    {
        void CreatePasswordHash(string password , out byte[] passwordHash , out byte[] passwordSalt);
        bool VerifyPasswordHash(string password , byte[] passwordHash , byte[] passwordSalt);
        string FindLocalPathFromUrl(string url);
    }
}
