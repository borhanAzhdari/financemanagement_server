﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MadPay.Common.Helpers.AppSetting
{
    public class CloudinarySettings
    {
        public string Cloud { get; set; }
        public string APIKey { get; set; }
        public string APISecret { get; set; }
    }
}
